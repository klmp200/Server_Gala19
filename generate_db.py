#!/usr/bin/env python3
import os
from datetime import datetime
from slugify import slugify
from random import randint

# To edit every year obviously, makes it match to your gala date
event_date = datetime(year=2019, month=11, day=16)


def datetime_helper(hour, minute):
    # Does not handle midnight and after
    return datetime(
        year=event_date.year,
        month=event_date.month,
        day=event_date.day,
        hour=hour,
        minute=minute,
    )


counters = ["Central", "Fosse", "Assidu", "Libanais", "Pop-corn", "Bulle"]
products = [
    {
        "name": "Bière âne mort",
        "price": 2.7,
        "categorie": "boissons",
        "sub_categorie": "bière",
        "counters": ["Central", "Fosse"],
        "happy_hours": [
            {
                "start": datetime_helper(21, 30),
                "end": datetime_helper(22, 30),
                "price": 2.2,
            }
        ],
    },
    {
        "name": "Bière Saison",
        "price": 3,
        "categorie": "boissons",
        "sub_categorie": "bière",
        "counters": ["Central", "Fosse"],
        "happy_hours": [
            {
                "start": datetime_helper(21, 30),
                "end": datetime_helper(22, 30),
                "price": 2.4,
            }
        ],
    },
    {
        "name": "Kastel red",
        "price": 3,
        "categorie": "boissons",
        "sub_categorie": "bière",
        "counters": ["Central", "Fosse"],
        "happy_hours": [
            {
                "start": datetime_helper(21, 30),
                "end": datetime_helper(22, 30),
                "price": 2.4,
            }
        ],
    },
    {
        "name": "Crémant",
        "price": 3.5,
        "categorie": "boissons",
        "sub_categorie": "Verre",
        "counters": ["Central", "Assidu"],
        "happy_hours": [
            {
                "start": datetime_helper(21, 30),
                "end": datetime_helper(22, 30),
                "price": 2.8,
            }
        ],
    },
    {
        "name": "Charmes (moelleux, vin blanc)",
        "price": 3.7,
        "categorie": "boissons",
        "sub_categorie": "Verre",
        "counters": ["Central", "Assidu"],
        "happy_hours": [
            {
                "start": datetime_helper(21, 30),
                "end": datetime_helper(22, 30),
                "price": 3,
            }
        ],
    },
    {
        "name": "Champagne",
        "price": 5,
        "categorie": "boissons",
        "sub_categorie": "Verre",
        "counters": ["Central", "Assidu"],
        "happy_hours": [
            {
                "start": datetime_helper(21, 30),
                "end": datetime_helper(22, 30),
                "price": 4,
            }
        ],
    },
    {
        "name": "Sirop",
        "price": 1,
        "categorie": "boissons",
        "sub_categorie": "Verre",
        "counters": ["Central", "Fosse", "Assidu"],
        "happy_hours": [
            {
                "start": datetime_helper(21, 30),
                "end": datetime_helper(22, 30),
                "price": 0.8,
            }
        ],
    },
    {
        "name": "Jus de pomme",
        "price": 2,
        "categorie": "boissons",
        "sub_categorie": "Verre",
        "counters": ["Central", "Fosse", "Assidu"],
        "happy_hours": [
            {
                "start": datetime_helper(21, 30),
                "end": datetime_helper(22, 30),
                "price": 1.6,
            }
        ],
    },
    {
        "name": "Limonade",
        "price": 2,
        "categorie": "boissons",
        "sub_categorie": "Verre",
        "counters": ["Central", "Fosse", "Assidu"],
        "happy_hours": [
            {
                "start": datetime_helper(21, 30),
                "end": datetime_helper(22, 30),
                "price": 1.6,
            }
        ],
    },
    {
        "name": "Crémant Bouteille",
        "price": 15.5,
        "categorie": "boissons",
        "sub_categorie": "Bouteille",
        "counters": ["Central", "Assidu"],
    },
    {
        "name": "Champagne Bouteille",
        "price": 25,
        "categorie": "boissons",
        "sub_categorie": "Bouteille",
        "counters": ["Central", "Assidu"],
    },
    {
        "name": "Charmes Bouteille (moelleux, vin blanc)",
        "price": 16,
        "categorie": "boissons",
        "sub_categorie": "Bouteille",
        "counters": ["Central", "Assidu"],
    },
    {
        "name": "Petite assiette",
        "price": 5,
        "categorie": "nourriture",
        "sub_categorie": "assiette",
        "counters": ["Libanais"],
    },
    {
        "name": "Grande assiette",
        "price": 10,
        "categorie": "nourriture",
        "sub_categorie": "assiette",
        "counters": ["Libanais"],
    },
    {
        "name": "Dessert",
        "price": 3,
        "categorie": "nourriture",
        "sub_categorie": "sucré",
        "counters": ["Libanais"],
    },
    {
        "name": "Pop-corn",
        "price": 1.5,
        "categorie": "nourriture",
        "sub_categorie": "sucré",
        "counters": ["Pop-corn"],
    },
    {
        "name": "Consigne",
        "price": 1,
        "categorie": "Consigne",
        "sub_categorie": "consigne",
        "counters": ["Central", "Assidu", "Fosse", "Libanais", "Pop-corn"],
    },
    {
        "name": "Déconsigne",
        "price": -1,
        "categorie": "Consigne",
        "sub_categorie": "consigne",
        "counters": ["Central", "Assidu", "Fosse", "Libanais", "Pop-corn"],
    },
]

generated_codes = {}


def generate_code(name):
    code = slugify(name).upper()
    while generated_codes.get(code, ""):
        code += str(randint(0, 9))
    generated_codes[code] = code
    return code


if __name__ == "__main__":
    if input("This will erase the current db, are you sure ? [y/N]\n") != "y":
        exit(0)
    print("Deleting database")
    if os.path.exists("app.db"):
        os.remove("app.db")

    print("Creating new database")
    # Import db here to create a new database after removal
    from galaapp.models import db, Product, Counter, Happy_Hour

    print("Creating counters")
    for counter in counters:
        print("Adding %s" % counter)
        db.session.add(Counter(name=counter))

    print("Creating products")
    for product in products:
        print("Adding %s" % product["name"])
        p = Product(
            code=generate_code(product["name"]),
            name=product["name"].capitalize(),
            price=product["price"],
            categorie=product["categorie"].capitalize(),
            sub_categorie=product["sub_categorie"].capitalize(),
        )
        # Get counters associated
        for counter in (
            db.session.query(Counter)
            .filter(Counter.name.in_(product["counters"]))
            .all()
        ):
            p.counters.append(counter)

        db.session.add(p)

        for hour in product.get("happy_hours", []):
            h = Happy_Hour(
                start=hour["start"],
                end=hour["end"],
                price=hour["price"],
                product_code=p.code,
            )
            db.session.add(h)

    # Actually add into the database
    db.session.commit()
